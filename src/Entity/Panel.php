<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PanelRepository")
 */
class Panel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $when_created;

    public function getId()
    {
        return $this->id;
    }

    public function getWhenCreated(): ?\DateTimeInterface
    {
        return $this->when_created;
    }

    public function setWhenCreated(\DateTimeInterface $when_created): self
    {
        $this->when_created = $when_created;

        return $this;
    }
}
